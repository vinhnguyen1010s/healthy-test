import axiosInstance from '../helpers/http-common';

const URI_CHART_MY_PAGE = `/chart-my-page`;
const URI_FOOD_MY_PAGE = `/food-my-page`;

class MyPageService {
  fetchDataChartMyPage = async () => {
    return await axiosInstance.get(URI_CHART_MY_PAGE);
  };

  fetchDataFoodMyPage = async (params) => {
    return await axiosInstance.get(URI_FOOD_MY_PAGE, { params });
  };
}

export default new MyPageService();
