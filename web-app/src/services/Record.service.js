import axiosInstance from '../helpers/http-common';

const URI_CHART_BODY_RECORD = `/chart-body-record`;
const URI_MY_EXERCISE = `/my-exercise`;
const URI_MY_DIARIES = `/my-diaries`;

class RecordService {
  fetchDataChartBodyRecord = async () => {
    return await axiosInstance.get(URI_CHART_BODY_RECORD);
  };

  fetchDataMyExercise = async () => {
    return await axiosInstance.get(URI_MY_EXERCISE);
  };

  fetchDataMyDiaries = async (params) => {
    return await axiosInstance.get(URI_MY_DIARIES, { params });
  };
}

export default new RecordService();
