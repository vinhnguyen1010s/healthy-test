import axiosInstance from '../helpers/http-common';

const URI_MY_COLUMN_PAGE = `/my-column-page`;

class ColumnPageService {
  fetchDataMyColumnPage = async (params) => {
    return await axiosInstance.get(URI_MY_COLUMN_PAGE, { params });
  };
}

export default new ColumnPageService();
