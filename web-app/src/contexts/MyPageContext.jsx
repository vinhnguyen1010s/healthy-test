import { createContext } from 'react';
import MyPageService from '../services/MyPage.service';

export const MyPageContext = createContext();

export const MyPageProvider = ({ children }) => {
  const getDataChartMyPage = () => {
    return MyPageService.fetchDataChartMyPage()
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const getDataFoods = (params) => {
    return MyPageService.fetchDataFoodMyPage(params)
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const dataMyPage = { getDataChartMyPage, getDataFoods };

  return <MyPageContext.Provider value={dataMyPage}>{children}</MyPageContext.Provider>;
};
