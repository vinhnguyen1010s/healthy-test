import { createContext } from 'react';
import ColumnPageService from '../services/ColumnPage.service';

export const MyColumnPageContext = createContext();

export const MyColumnPageProvider = ({ children }) => {
  const getDataChartColumnPage = (params) => {
    return ColumnPageService.fetchDataMyColumnPage(params)
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const dataColumnPage = { getDataChartColumnPage };

  return <MyColumnPageContext.Provider value={dataColumnPage}>{children}</MyColumnPageContext.Provider>;
};
