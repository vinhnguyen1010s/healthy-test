import { createContext } from 'react';
import RecordService from '../services/Record.service';

export const RecordContext = createContext();

export const RecordProvider = ({ children }) => {
  const getDataChartRecords = () => {
    return RecordService.fetchDataChartBodyRecord()
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const getDataMyExercises = () => {
    return RecordService.fetchDataMyExercise()
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const getDataMyDiaries = (params) => {
    return RecordService.fetchDataMyDiaries(params)
      .then((res) => {
        if (res && res.data) {
          return res.data;
        }
      })
      .catch((error) => {
        return error;
      });
  };

  const dataRecord = { getDataChartRecords, getDataMyExercises, getDataMyDiaries };

  return <RecordContext.Provider value={dataRecord}>{children}</RecordContext.Provider>;
};
