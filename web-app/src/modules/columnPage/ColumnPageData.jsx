import { Box, Button, Divider, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/system/Unstable_Grid';
import React, { useContext, useEffect, useState } from 'react';
import { LoadingSpinner } from '../../components/LoadingSpinner';
import { MyColumnPageContext } from '../../contexts/MyColumnPageContext';
import themes, { colors } from '../../themes/themes';
import './columnPage.scss';

const useStyles = makeStyles(() => ({
  contentPage: {
    flexGrow: 1,
    display: 'block',
    padding: themes.spacing(3, 20),

    [themes.breakpoints.down('md')]: {
      padding: themes.spacing(3, 4),
    },
  },

  textItem: {
    position: 'relative',
    bottom: '30px',
    left: 0,
  },

  itemTime: {
    padding: '2px 4px',
    color: colors.colorText,
    backgroundColor: colors.light,
  },
}));

const recommended = [
  { id: 1, recommended: 'RECOMMENDED COLUMN', title_commended: 'オススメ' },
  { id: 2, recommended: 'RECOMMENDED DIET', title_commended: 'ダイエット' },
  { id: 3, recommended: 'RECOMMENDED BEAUTY', title_commended: '美容' },
  { id: 4, recommended: 'RECOMMENDED HEALTHY', title_commended: '健康' },
];

const ColumnPageData = () => {
  const classes = useStyles();
  const LIMIT = 8;
  const [isLoading, setIsLoading] = useState(false);

  const { getDataChartColumnPage } = useContext(MyColumnPageContext);

  const [columnData, setColumnData] = useState([]);
  const [pageNum, setPageNum] = useState(1);

  const getDataColumnPage = () => {
    const params = {
      _page: pageNum,
      _limit: LIMIT,
    };
    setIsLoading(true);
    getDataChartColumnPage(params)
      .then((res) => {
        if (res && res.data) {
          setColumnData((prevItems) => [...prevItems, ...res.data]);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleLoadMore = () => {
    setPageNum((prevPage) => prevPage + 1);
  };

  useEffect(() => {
    getDataColumnPage();

    return () => {
      // second
    };
  }, [pageNum]);

  return (
    <>
      {isLoading ? <LoadingSpinner /> : null}
      <Box className={classes.contentPage}>
        <Box sx={{ py: 4, mt: 4 }}>
          <Grid container spacing={2}>
            {recommended.map((item) => (
              <Grid xs={6} sm={6} md={3} key={item.id}>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    bgcolor: colors.line1,
                    p: 2,
                  }}
                >
                  <Typography
                    sx={{
                      maxWidth: 220,
                      fontSize: '22px',
                      color: colors.light,
                      textAlign: 'center',
                      [themes.breakpoints.down('lg')]: {
                        fontSize: '16px',
                      },
                    }}
                  >
                    {item.recommended}
                  </Typography>
                  <Divider sx={{ width: '30%', bgcolor: colors.colorText, my: 1.25, height: '2px' }} />
                  <Typography
                    variant="subtitle1"
                    color={'white'}
                    sx={{
                      [themes.breakpoints.down('lg')]: {
                        fontSize: '14px',
                      },
                    }}
                  >
                    {item.title_commended}
                  </Typography>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>

        {/*  */}
        <Box sx={{ py: 3 }}>
          {columnData.length > 0 ? (
            <Grid container spacing={1}>
              {columnData.map((item, index) => (
                <Grid xs={6} sm={6} md={3} key={item.id}>
                  <Box sx={{ width: '100%', height: '82%' }}>
                    <img
                      src={item.image_url}
                      alt={item.point_time}
                      style={{ width: '100%', height: '82%', maxWidth: '100%', maxHeight: '100%', objectFit: 'cover' }}
                    />

                    <Box className={classes.textItem}>
                      <Typography variant="subtitle9" className={classes.itemTime}>
                        {`${item.date_time} ${item.point_time}`}
                      </Typography>
                      <Typography variant="subtitle3" className="content-page">
                        {item.content_text}
                      </Typography>

                      <Box key={index} sx={{ display: 'flex', gap: 1 }}>
                        {item.tags.length > 0 &&
                          item.tags.map((tag, index) => (
                            <Typography variant="subtitle4" sx={{ color: colors.primary }}>{`#${tag}`}</Typography>
                          ))}
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              ))}
            </Grid>
          ) : (
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="subtitle6">Not found data</Typography>
            </Box>
          )}
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 2 }}>
          <Button variant="contained" sx={{ p: 2, width: 296 }} onClick={handleLoadMore}>
            {'コラムをもっと見る'}
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default ColumnPageData;
