import React from 'react';
import ColumnPageData from './ColumnPageData';
import { MyColumnPageProvider } from '../../contexts/MyColumnPageContext';

const ColumnPage = () => {
  return (
    <>
      <MyColumnPageProvider>
        <ColumnPageData />
      </MyColumnPageProvider>
    </>
  );
};

export default ColumnPage;
