import { Box, Button, Divider, Stack, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/system/Unstable_Grid';
import React, { useContext, useEffect, useState } from 'react';
import themes, { colors, primary } from '../../themes/themes';
import './myPage.scss';
import LineChart from '../../components/chart/LineChart';
import { RecordContext } from '../../contexts/RecordContext';
import { LoadingSpinner } from '../../components/LoadingSpinner';

const useStyles = makeStyles(() => ({
  contentPage: {
    display: 'flex',
    flexDirection: 'column',
    gap: '56px',
    padding: themes.spacing(3, 20),

    [themes.breakpoints.down('md')]: {
      padding: themes.spacing(3, 4),
    },
  },

  textItem: {
    position: 'relative',
    bottom: '50%',
    textAlign: 'center',
  },
  itemTime: {
    padding: '2px 4px',
    color: colors.colorText,
    backgroundColor: colors.light,
  },
  titleItem: {
    position: 'relative',
    bottom: `calc(50% + 42px)`,
    padding: '2px 4px',
    color: colors.colorText,
  },

  buttonSelected: {
    padding: '4px 12px',
    width: '50px',
    borderRadius: '18px',
    backgroundColor: colors.light,
    color: colors.colorText,
    '&:hover': {
      cursor: 'pointer',
    },
  },

  button: {
    padding: '4px 12px',
    width: '50px',
    borderRadius: '18px',
    backgroundColor: colors.colorText,
    color: colors.light,
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

const dataItems = [
  {
    id: 1,
    title: 'BODY RECORD',
    text: '自分のカラダの記録',
    image_url: 'assets/images/imgs/MyRecommend-1.jpg',
  },
  {
    id: 2,
    title: 'MY EXERCISE',
    text: '自分の運動の記録',
    image_url: 'assets/images/imgs/MyRecommend-2.jpg',
  },
  {
    id: 3,
    title: 'MY DIARY',
    text: '自分の日記',
    image_url: 'assets/images/imgs/MyRecommend-3.jpg',
  },
];

const selectedTime = [
  { id: 1, name: 'day', title: '日' },
  { id: 2, name: 'week', title: '週' },
  { id: 3, name: 'month', title: '月' },
  { id: 4, name: 'year', title: '年' },
];

const MyRecordData = () => {
  const classes = useStyles();
  const LIMIT = 8;
  const [isLoading, setIsLoading] = useState(false);

  const { getDataChartRecords, getDataMyExercises, getDataMyDiaries } = useContext(RecordContext);
  const [chartRecord, setChartRecord] = useState({});
  const [myExercise, setMyExercise] = useState([]);
  const [myDiaries, setMyDiaries] = useState([]);
  const [pageNum, setPageNum] = useState(1);
  const [timeSeleted, setTimeSeleted] = useState(selectedTime[3].name);

  const handleSelectedTime = (item) => {
    setTimeSeleted(item.name);
  };

  const getDataBodyRecord = () => {
    setIsLoading(true);
    getDataChartRecords()
      .then((res) => {
        if (res && res.data) {
          setChartRecord(res.data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const getDataMyExercise = () => {
    setIsLoading(true);
    getDataMyExercises()
      .then((res) => {
        if (res) {
          setMyExercise(res);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleGetDataMyDiaries = () => {
    const params = {
      _page: pageNum,
      _limit: LIMIT,
    };
    setIsLoading(true);
    getDataMyDiaries(params)
      .then((res) => {
        if (res) {
          console.log('res:::====', res);
          setMyDiaries((prevItems) => [...prevItems, ...res.data]);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleLoadMore = () => {
    setPageNum((prevPage) => prevPage + 1);
  };

  useEffect(() => {
    getDataMyExercise();
    getDataBodyRecord();
  }, []);

  useEffect(() => {
    handleGetDataMyDiaries();
    // first
  }, [pageNum]);

  return (
    <>
      {isLoading ? <LoadingSpinner /> : null}
      <Box sx={{ flexGrow: 1 }} className={classes.contentPage}>
        <Box>
          <Grid container spacing={6}>
            {dataItems.map((item) => (
              <Grid xs={4} sm={4} key={item.id}>
                <Box sx={{ width: '100%', height: '100%', p: 3, backgroundColor: colors.light }}>
                  <Box className="image-container">
                    <img className="image" src={item.image_url} alt={item.text} />
                  </Box>
                  <Box className={classes.titleItem}>
                    <Box sx={{ textAlign: 'center', py: 0.4 }}>
                      <Typography variant="subtitle6" sx={{ color: colors.light, textAlign: 'center' }}>
                        {item.title}
                      </Typography>
                    </Box>
                    <Box sx={{ textAlign: 'center', py: 0.4 }}>
                      <Typography
                        variant="subtitle9"
                        sx={{ bgcolor: colors.primary, textAlign: 'center', px: 2, py: 0.5 }}
                      >
                        {item.text}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>

        <Box
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            gap: 2,
            backgroundColor: colors.line1,
          }}
        >
          <Box sx={{ display: 'flex', gap: 1, alignItems: 'center', color: colors.colorText }}>
            <Typography variant="subtitle9" sx={{ maxWidth: 100 }}>
              BODY RECORD
            </Typography>
            <Typography variant="subtitle7" sx={{ color: colors.colorText }}>
              {chartRecord.time}
            </Typography>
          </Box>
          <LineChart data={chartRecord}></LineChart>
          <Box sx={{ display: 'flex', gap: 2 }}>
            {selectedTime.map((btn) => (
              <button
                key={btn.id}
                size="small"
                className={btn.name === timeSeleted ? classes.buttonSelected : classes.button}
                onClick={() => handleSelectedTime(btn)}
              >
                {btn.title}
              </button>
            ))}
          </Box>
        </Box>

        <Box sx={{ backgroundColor: colors.line1, p: 3 }}>
          <Box sx={{ display: 'flex', gap: 1, alignItems: 'center', color: colors.colorText, pb: 1 }}>
            <Typography variant="subtitle9" sx={{ maxWidth: 90, wordWrap: 'break-word' }}>
              MY EXERCISE
            </Typography>
            <Typography variant="subtitle7" sx={{ color: colors.colorText }}>
              {myExercise.date}
            </Typography>
          </Box>
          <Box sx={{ height: 210, overflowY: 'auto', pr: 4 }}>
            {myExercise.content?.length > 0 ? (
              <Grid container columnSpacing={6}>
                {myExercise.content.map((item, index) => (
                  <Grid xs={12} sm={6} key={index}>
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', py: 0.4 }}>
                      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                        <Typography variant="subtitle3" sx={{ color: colors.colorText }}>
                          <li>{item.exercise}</li>
                        </Typography>
                        <Typography variant="subtitle9" sx={{ color: colors.light, ml: 2.5 }}>
                          {item.kcal}
                        </Typography>
                      </Box>
                      <Typography variant="subtitle9" sx={{ color: colors.light }}>
                        {item.time}
                      </Typography>
                    </Box>
                    <Divider sx={{ height: '1.5px', bgcolor: colors.line3 }} />
                  </Grid>
                ))}
              </Grid>
            ) : (
              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="subtitle6">Not found data</Typography>
              </Box>
            )}
          </Box>
        </Box>
        {/* My diary */}

        <Box>
          <Typography variant="subtitle9" sx={{ maxWidth: 90, wordWrap: 'break-word' }}>
            MY DIARY
          </Typography>

          {myDiaries.length > 0 ? (
            <Grid container spacing={1}>
              {myDiaries.map((item, index) => (
                <Grid xs={6} sm={3} key={index}>
                  <Box sx={{ p: 2, border: `1px solid ${colors.line1}` }}>
                    <Stack sx={{ pb: 0.5 }}>
                      <Typography variant="subtitle8" sx={{}}>
                        {item.date}
                      </Typography>
                      <Typography variant="subtitle8" sx={{}}>
                        {item.time_point}
                      </Typography>
                    </Stack>
                    <Typography variant="subtitle4">{item.title}</Typography>
                    <Typography variant="subtitle4" className="content-page">
                      {item.content}
                    </Typography>
                  </Box>
                </Grid>
              ))}
            </Grid>
          ) : (
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="subtitle6">Not found data</Typography>
            </Box>
          )}
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 2 }}>
          <Button variant="contained" sx={{ p: 2, width: 296 }} onClick={handleLoadMore}>
            {'記録をもっと見る'}
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default MyRecordData;
