import React from 'react';
import MyRecordData from './MyRecordData';
import { RecordProvider } from '../../contexts/RecordContext';

const MyRecord = () => {
  return (
    <RecordProvider>
      <MyRecordData />
    </RecordProvider>
  );
};

export default MyRecord;
