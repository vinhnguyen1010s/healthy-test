import React from 'react';
import { MyPageProvider } from '../../contexts/MyPageContext';
import TopPage from './DataMyPage';

const MyPage = () => {
  return (
    <>
      <MyPageProvider>
        <TopPage />
      </MyPageProvider>
    </>
  );
};

export default MyPage;
