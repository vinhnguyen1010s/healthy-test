import { Box, Button, CardMedia, IconButton, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/system/Unstable_Grid';
import React, { useContext, useEffect, useState } from 'react';
import CustomCircular from '../../components/chart/CustomCircular';
import LineChart from '../../components/chart/LineChart';
import themes, { colors } from '../../themes/themes';
import { MyPageContext } from '../../contexts/MyPageContext';
import { LoadingSpinner } from '../../components/LoadingSpinner';

const useStyles = makeStyles(() => ({
  contentPage: {
    flexGrow: 1,
    display: 'block',
    padding: themes.spacing(3, 20),

    [themes.breakpoints.down('md')]: {
      padding: themes.spacing(3, 4),
    },
  },

  titleItem: {
    position: 'relative',
    bottom: '30px',
    left: 0,
    padding: '2px 4px',
    color: colors.colorText,
    backgroundColor: colors.light,
  },
}));

const buttons = [
  { id: 1, imageUrl: 'assets/images/icons/dinner.svg', name: 'morning' },
  { id: 2, imageUrl: 'assets/images/icons/lunch.svg', name: 'lunch' },
  { id: 3, imageUrl: 'assets/images/icons/dinner.svg', name: 'dinner' },
  { id: 4, imageUrl: 'assets/images/icons/snack.svg', name: 'snack' },
];

const DataMyPage = () => {
  const classes = useStyles();
  const LIMIT = 8;

  const { getDataChartMyPage, getDataFoods } = useContext(MyPageContext);

  const [isLoading, setIsLoading] = useState(false);
  const [pageNum, setPageNum] = useState(1);

  const [foodList, setFoodList] = useState([]);
  const [dataChart, setDataChart] = useState({});

  const handleGetChartFood = () => {
    setIsLoading(true);
    getDataChartMyPage()
      .then((res) => {
        setDataChart(res);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleGetDataFoods = () => {
    const params = {
      _page: pageNum,
      _limit: LIMIT,
    };
    setIsLoading(true);
    getDataFoods(params)
      .then((res) => {
        if (res && res.data) {
          setFoodList((prevItems) => [...prevItems, ...res.data]);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleLoadMore = () => {
    setPageNum((prevPage) => prevPage + 1);
  };

  useEffect(() => {
    handleGetChartFood();
  }, []);

  useEffect(() => {
    handleGetDataFoods();
  }, [pageNum]);

  return (
    <>
      {isLoading ? <LoadingSpinner /> : null}
      <Box sx={{ flexGrow: 1 }}>
        <Box sx={{ display: 'flex', height: 312 }}>
          <Box sx={{ width: '42%' }}>
            <img
              style={{ height: '100%', width: '100%', backgroundSize: 'cover', objectFit: 'cover' }}
              src={dataChart.image_url}
            />
            <CustomCircular data={dataChart} />
          </Box>
          <Box sx={{ p: 2, display: 'flex', justifyContent: 'center', width: '58%', backgroundColor: colors.line1 }}>
            <LineChart data={dataChart}></LineChart>
          </Box>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 3 }}>
          <Box sx={{ display: 'flex', gap: 5 }}>
            {buttons.map((item) => (
              <IconButton key={item.id} style={{ maxWidth: '136px', height: 'auto' }}>
                <img style={{ width: '100%', height: '100%' }} src={item.imageUrl} alt={item.alt} />
              </IconButton>
            ))}
          </Box>
        </Box>

        <Box className={classes.contentPage}>
          {foodList.length > 0 ? (
            <Grid container spacing={1}>
              {foodList.map((item, index) => (
                <Grid xs={6} sm={3} key={index}>
                  <Box sx={{ width: '100%', height: '100%' }}>
                    <img
                      src={item.imageUrl}
                      alt={item.point_time}
                      style={{ width: '100%', height: '100%', maxWidth: '100%', maxHeight: '100%', objectFit: 'cover' }}
                    />
                    <Typography variant="subtitle9" className={classes.titleItem}>
                      {`${item.time}:${item.point_time}`}
                    </Typography>
                  </Box>
                </Grid>
              ))}
            </Grid>
          ) : (
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="subtitle6">Not found data</Typography>
            </Box>
          )}
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 8 }}>
          <Button variant="contained" sx={{ p: 2, width: 296 }} onClick={handleLoadMore}>
            {'記録をもっと見る'}
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default DataMyPage;
