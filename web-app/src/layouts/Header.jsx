import {
  AppBar,
  Badge,
  Grid,
  IconButton,
  ListItem,
  ListItemIcon,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useStyles } from './layoutStyles';
import './layout.scss';
import logo from '../assets/images/logo.svg';
import challenge from '../assets/images/icons/icon_challenge.svg';
import memo from '../assets/images/icons/icon_memo.svg';
import info from '../assets/images/icons/icon_info.svg';
import { styled } from '@mui/material/styles';
import MuiListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import { colors } from '../themes/themes';

export const navLinks = [
  { title: `自分の記録`, path: `/my-record`, name: 'record', icon: memo, badge: false },
  { title: `チャレンジ`, path: `/challenge`, name: 'challenge', icon: challenge, badge: false },
  { title: `お知らせ`, path: `/notices`, name: 'notices', icon: info, badge: true },
];

const ListItemText = styled(MuiListItemText)({
  '& .MuiListItemText-primary': {
    fontWeight: 300,
  },
});

const menuItems = [
  { id: 1, path: '/my-record', title: '自分の記録' },
  { id: 2, path: '/weight-graph', title: '体重グラフ' },
  { id: 3, path: '/target', title: '目標' },
  { id: 4, path: '/selected-course', title: '選択中のコース' },
  { id: 5, path: '/column-page', title: 'コラム一覧' },
  { id: 6, path: '/setting', title: '設定' },
];
const Header = () => {
  const classes = useStyles();
  // const links = navLinks;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClickMenu = (event) => {
    // if(event.)
    console.log('event:::', event);
  };
  return (
    <>
      <AppBar position="fixed" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Link to="/" className={classes.typography}>
            <Typography component="img" sx={{ px: 2, py: 1 }} src={logo} className="logo-app"></Typography>
          </Link>

          <Box className="navlinks">
            <nav className={classes.navDisplayFlex}>
              <NavLink className={classes.navItems} onClick={handleClick}>
                <ListItem className={classes.linkItem}>
                  <ListItemIcon className="icon-challenge">
                    <img src={memo} />
                  </ListItemIcon>
                  <ListItemText primary={'自分の記録'} />
                </ListItem>
              </NavLink>
              <NavLink className={classes.navItems}>
                <ListItem className={classes.linkItem}>
                  <ListItemIcon className="icon-challenge">
                    <img src={challenge} />
                  </ListItemIcon>
                  <ListItemText primary={'チャレンジ'} />
                </ListItem>
              </NavLink>
              <NavLink className={classes.navItems}>
                <ListItem className={classes.linkItem}>
                  <ListItemIcon className="icon-challenge">
                    <Badge badgeContent={4} color="primary">
                      <img src={info} />
                    </Badge>
                  </ListItemIcon>
                  <ListItemText primary={'お知らせ'} />
                </ListItem>
              </NavLink>
              {/* {links.map((item, index) => (
                <NavLink className={classes.navItems} to={item.path} key={index} onClick={() => handleClickMenu(item)}>
                  <ListItem className={classes.linkItem} key={item.index}>
                    <ListItemIcon className="icon-challenge">
                      {item.badge ? (
                        <Badge badgeContent={4} color="primary">
                          <img src={item.icon} />
                        </Badge>
                      ) : (
                        <img src={item.icon} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={item.title} />
                  </ListItem>
                </NavLink>
              ))} */}

              <Menu
                id="demo-positioned-menu"
                aria-labelledby="demo-positioned-button"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                sx={{
                  '& .MuiPaper-root': {
                    minWidth: 280,
                    backgroundColor: colors.line3,
                    color: colors.colorText,
                  },
                }}
              >
                {menuItems.map((menu, idx) => (
                  <MenuItem onClick={handleClose} key={idx}>
                    <NavLink to={menu.path} style={{ textDecoration: 'none', width: '100%' }}>
                      {menu.title}
                    </NavLink>
                  </MenuItem>
                ))}
              </Menu>
              <Box sx={{ pl: 2 }}>
                <IconButton>
                  <MenuIcon color="primary"></MenuIcon>
                </IconButton>
              </Box>
            </nav>
          </Box>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
