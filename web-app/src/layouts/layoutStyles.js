import { makeStyles } from '@mui/styles';
import themes, { colors } from '../themes/themes';

const primary = themes.palette.primary;
export const useStyles = makeStyles(() => ({
  appBar: {
    backgroundColor: colors.line2,
  },

  toolbar: {
    // height: '3.125em',
    // minHeight: 'unset',
    // display: 'flex',
    // padding: themes.spacing(0, 72),
    // ...themes.mixins.toolbar,
    // '& a': {
    //   color: 'black',
    //   textDecoration: 'none',
    // },z
    backgroundColor: colors.line2,
    '&.MuiToolbar-root': {
      display: 'flex',
      justifyContent: 'space-between',
      padding: themes.spacing(0, 20),

      [themes.breakpoints.down('md')]: {
        padding: themes.spacing(0, 4),
      },
    },
  },

  typography: {
    display: 'flex',
    alignItems: 'center',
    whiteSpace: 'nowrap',
    height: '100%',
  },

  navlinks: {
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  navDisplayFlex: {
    display: 'flex',
    justifyContent: `space-between`,
    alignItems: 'center',
    padding: themes.spacing(0),
  },

  navItems: {
    textDecoration: `none`,

    [themes.breakpoints.up('sm')]: {
      margin: '0 2px',
    },

    '&.active': {
      // color: primary.main,
    },

    '&:hover': {
      color: primary.main,
    },

    '&:focus': {
      color: 'none',
    },
  },

  linkItem: {
    display: `flex`,
    flexGrow: 1,
    justifyContent: `space-between`,
    textDecoration: `none`,
    '&.MuiListItem-root': {
      padding: themes.spacing(0, 1),
    },
    '& .MuiListItemIcon-root': {
      minWidth: 'unset',
    },
  },

  content: {
    position: 'relative',
    top: '64px',
    // fontSize: '0.875rem',
    backgroundColor: colors.colorText,
    minHeight: `calc(100vh - 192px)`,
  },

  footer: {
    position: 'relative',
    bottom: '-64px',
    backgroundColor: colors.line2,
    height: '128px',
    marginTop: '1rem',
    padding: themes.spacing(0, 20),
  },
}));
