export const stringAvatar = (firstName, lastName) => {
  if (firstName && lastName) {
    let last_name = removeVietnameseTones(lastName?.charAt(0)).toLocaleUpperCase();
    let first_name = removeVietnameseTones(firstName?.charAt(0)).toLocaleUpperCase();
    return `${first_name}${last_name}`;
  }
  return '';
};
