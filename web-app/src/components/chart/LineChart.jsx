import React from 'react';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { colors } from '../../themes/themes';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false,
      // position: 'top' as const,
    },
    title: {
      display: false,
      // text: 'Chart.js Line Chart',
    },
  },
  scales: {
    y: {
      display: false, // Hide y-axis labels
    },
    x: {
      grid: {
        color: colors.colorText, // Change the color of the x-axis grid lines
      },
      ticks: {
        color: colors.colorText,
        font: {
          size: 14, // Set the font size for y-axis labels
        },
      },
    },
  },
};

const LineChart = (props) => {
  const { data } = props;

  const labels = ['6月', '7月', '8月', '9月', '10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月'];

  const dataChart = {
    labels: labels,
    datasets: [
      {
        // label: 'Dataset 1',
        data: [110, 100, 80, 78, 65, 60, 50, 45, 40, 32, 26, 28],
        borderColor: colors.light,
      },
      {
        // label: 'Dataset 2',
        data: [110, 105, 70, 88, 75, 70, 60, 75, 60, 55, 50, 58],
        borderColor: colors.secondary,
      },
    ],
  };
  // const dataChart = {};
  return (
    <>
      <Line data={dataChart} options={options} />
    </>
  );
};

export default LineChart;
