import { Box, CircularProgress, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { colors } from '../../themes/themes';

const CustomCircular = (props) => {
  const { data } = props;

  return (
    <>
      <Box
        sx={{
          position: 'relative',
          display: 'inline-flex',
          minHeight: 150,
          minWidth: 150,
          position: 'relative',
          top: '-75%',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
        }}
      >
        <CircularProgress
          size={160}
          thickness={1.5}
          variant="determinate"
          value={data?.index_num ?? 0}
          sx={{ color: colors.colorText }}
        />
        <Box
          sx={{
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            position: 'absolute',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            gap: 1,
          }}
        >
          <Typography variant="subtitle8" color={colors.colorText}>
            {data?.time}
          </Typography>
          <Typography variant="subtitle6" color={colors.colorText}>
            {`${data?.index_num ? data?.index_num : 0}%`}
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export default CustomCircular;
