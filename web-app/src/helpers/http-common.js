import axios from 'axios';

export const WEB_APP_API = 'http://localhost:8081/api';

export default axios.create({
  baseURL: WEB_APP_API,
  header: {
    'Content-Type': 'application/json',
  },
});
