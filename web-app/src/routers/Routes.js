import React from 'react';

const MyPage = React.lazy(() => import('../modules/myPage/MyPage'));
const MyRecord = React.lazy(() => import('../modules/myRecord/MyRecord'));
const ColumnPage = React.lazy(() => import('../modules/columnPage/ColumnPage'));
const Notices = React.lazy(() => import('../modules/notices/Notices'));
const Challenge = React.lazy(() => import('../modules/challenge/Challenge'));

export const componentRoutes = [
  {
    path: '/my-page',
    component: <MyPage />,
  },
  {
    path: '/my-record',
    component: <MyRecord />,
  },
  {
    path: '/notices',
    component: <Notices />,
  },
  {
    path: '/challenge',
    component: <Challenge />,
  },
  {
    path: '/column-page',
    component: <ColumnPage />,
  },
];
