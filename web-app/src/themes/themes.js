import { createTheme } from '@mui/material';

export const primary = {
  main: '#ff963c',
  light: '#FFCC21',
  dark: '#EA6C00',
};

export const colors = {
  primary: '#FF963C',
  dark: '#EA6C00',
  light: '#FFCC21',
  secondary: '#8FE9D0',
  colorText: '#FFFFFF',
  line1: '#2E2E2E',
  line2: '#414141',
  line3: '#777777',
  error: '#DC4340',
  bgColor: '#FFEFEF',
};

const themes = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 640, // Handset portrait  (small, medium, large) | handset landscape (small)
      md: 960, // handset landcapse (medium, large) | tablet portrait (smallm large)
      lg: 1280, // tablet landscape (small, large)
      xl: 1600, // large desktops
    },
  },

  palette: {
    primary: {
      main: primary.main,
      light: primary.light,
      dark: primary.dark,
      contrastText: '#FFFFFF',
    },

    active: {
      color: colors.primary,
    },

    text: {
      main: colors.main,
      primary: colors.primary,
      secondary: colors.secondary,
      error: colors.error,
    },

    divider: colors.line2,

    background: {
      default: '#FFFFFF',
    },

    colors: {
      line1: '#2E2E2E',
      line2: '#414141',
      line3: '#777777',
    },
  },

  typography: {
    fontFamily: ['Nunito', 'HiraKaku', 'Inter'].join(','),

    // font JP
    subtitle1: {
      fontWeight: 300,
      fontSize: '18px',
      lineHeight: '26px',
      color: colors.colorText,
      fontFamily: 'Nunito',
    },

    subtitle2: {
      fontWeight: 300,
      fontSize: '16px',
      lineHeight: '23px',
      color: colors.colorText,
      fontFamily: 'Nunito',
    },

    subtitle3: {
      fontWeight: 300,
      fontSize: '15px',
      color: colors.line1,
      fontFamily: 'Nunito',
    },
    subtitle31: {
      fontWeight: 300,
      fontSize: '14px',
      color: colors.colorText,
      fontFamily: 'Nunito',
    },

    subtitle4: {
      fontWeight: 300,
      fontSize: '12px',
      lineHeight: '16px',
      color: colors.line2,
      fontFamily: 'Nunito',
    },

    subtitle5: {
      fontWeight: 300,
      fontSize: '11px',
      lineHeight: '16px',
      fontFamily: 'Nunito',
    },

    // fonts EN
    subtitle6: {
      fontWeight: 400,
      fontSize: '25px',
      lineHeight: '30px',
      color: colors.light,
      fontFamily: 'Inter',
    },

    subtitle7: {
      fontWeight: 400,
      fontSize: '22px',
      lineHeight: '27px',
      color: colors.line1,
      fontFamily: 'Inter',
    },

    subtitle8: {
      fontWeight: 400,
      fontSize: '18px',
      fontFamily: 'Inter',
    },

    subtitle9: {
      fontWeight: 400,
      fontSize: '15px',
      lineHeight: '18px',
      fontFamily: 'Inter',
    },
  },
});

export default themes;
