const faker = require("faker");
const fs = require("fs");

const dataChartTopPage = {
  image_url: "assets/images/imgs/d01.jpg",
  index_num: 75,
  time: "05.21",
  point_time: "Lunch",
  colaries: [110, 100, 80, 78, 65, 60, 50, 45, 40, 32, 26, 12],
  kcal: [110, 105, 70, 88, 75, 70, 60, 75, 60, 55, 50, 58],
};

const dataFoodMyPage = [
  {
    id: 1,
    point_time: "Morning",
    time: "05: 21",
    imageUrl: "assets/images/imgs/m01.jpg",
  },
  {
    id: 2,
    point_time: "Dinner",
    time: "05: 21",
    imageUrl: "assets/images/imgs/d01.jpg",
  },
  {
    id: 3,
    point_time: "Lunch",
    time: "05: 21",
    imageUrl: "assets/images/imgs/l01.jpg",
  },
  {
    id: 4,
    point_time: "Snack",
    time: "05: 21",
    imageUrl: "assets/images/imgs/s01.jpg",
  },
  {
    id: 5,
    point_time: "Morning",
    time: "05: 21",
    imageUrl: "assets/images/imgs/m02.jpg",
  },
  {
    id: 6,
    point_time: "Lunch",
    time: "05: 21",
    imageUrl: "assets/images/imgs/l02.jpg",
  },
  {
    id: 7,
    point_time: "Dinner",
    time: "05: 21",
    imageUrl: "assets/images/imgs/d02.jpg",
  },
  {
    id: 8,
    point_time: "Snack",
    time: "05: 21",
    imageUrl: "assets/images/imgs/s01.jpg",
  },
  {
    id: 9,
    point_time: "Morning",
    time: "05: 21",
    imageUrl: "assets/images/imgs/m03.jpg",
  },
  {
    id: 10,
    point_time: "Lunch",
    time: "05: 21",
    imageUrl: "assets/images/imgs/l03.jpg",
  },
  {
    id: 11,
    point_time: "Dinner",
    time: "05: 21",
    imageUrl: "assets/images/imgs/d01.jpg",
  },
  {
    id: 12,
    point_time: "Snack",
    time: "05: 21",
    imageUrl: "assets/images/imgs/s01.jpg",
  },
  {
    id: 13,
    point_time: "Morning",
    time: "05: 21",
    imageUrl: "assets/images/imgs/m01.jpg",
  },
  {
    id: 14,
    point_time: "Lunch",
    time: "05: 21",
    imageUrl: "assets/images/imgs/l02.jpg",
  },
  {
    id: 15,
    point_time: "Dinner",
    time: "05: 21",
    imageUrl: "assets/images/imgs/d02.jpg",
  },
  {
    id: 16,
    point_time: "Snack",
    time: "05: 21",
    imageUrl: "assets/images/imgs/s01.jpg",
  },
];

const chartBodyRecord = {
  data: {
    title: "Body Record",
    index_num: 75,
    time: "2021.05.21",
    point_time: "Lunch",
    colaries: [110, 100, 80, 78, 65, 60, 50, 45, 40, 32, 26, 12],
    kcal: [110, 105, 70, 88, 75, 70, 60, 75, 60, 55, 50, 58],
  },
};

const dataMyExercise = {
  title: "My Exercise",
  date: "2021.05.21",
  content: [
    {
      id: 1,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い))",
      time: "10 min",
    },
    {
      id: 2,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 3,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 4,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 5,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 6,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 7,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 8,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 9,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 10,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 11,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 12,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 13,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 14,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 15,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
    {
      id: 16,
      kcal: "26kcal",
      exercise: "家事全般（立位・軽い)",
      time: "10 min",
    },
  ],
};

const dataMyDiaries = [
  {
    id: 1,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 2,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 3,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 4,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 5,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 6,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 7,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 8,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 9,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 10,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 11,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 12,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 13,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 14,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 15,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 16,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 17,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
  {
    id: 18,
    date: "2021.05.21",
    time_point: "23:25",
    title: "私の日記の記録が一部表示されます",
    content:
      "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
  },
];

const dataColumnPage = [
  {
    id: 1,
    image_url: "assets/images/imgs/column-1.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 2,
    image_url: "assets/images/imgs/column-2.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 3,
    image_url: "assets/images/imgs/column-3.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 4,
    image_url: "assets/images/imgs/column-4.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 5,
    image_url: "assets/images/imgs/column-5.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 6,
    image_url: "assets/images/imgs/column-6.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 7,
    image_url: "assets/images/imgs/column-7.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 8,
    image_url: "assets/images/imgs/column-8.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 9,
    image_url: "assets/images/imgs/column-1.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 10,
    image_url: "assets/images/imgs/column-2.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 11,
    image_url: "assets/images/imgs/column-3.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 12,
    image_url: "assets/images/imgs/column-4.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 13,
    image_url: "assets/images/imgs/column-5.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 14,
    image_url: "assets/images/imgs/column-6.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 15,
    image_url: "assets/images/imgs/column-7.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 16,
    image_url: "assets/images/imgs/column-8.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 17,
    image_url: "assets/images/imgs/column-1.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
  {
    id: 18,
    image_url: "assets/images/imgs/column-2.jpg",
    date_time: "2021.05.17",
    point_time: "23:25",
    content_text:
      "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ魚を食べるメリ",
    tags: ["魚料理", "和食", "DHA"],
  },
];

(() => {
  // generator data json
  const db = {
    "chart-my-page": dataChartTopPage,
    "food-my-page": dataFoodMyPage,
    "chart-body-record": chartBodyRecord,
    "my-exercise": dataMyExercise,
    "my-diaries": dataMyDiaries,
    "my-column-page": dataColumnPage,

    profile: {
      name: "Vinh Nguyen",
    },
  };

  // Write DB object to db.json
  fs.writeFile("./src/db.json", JSON.stringify(db), () => {
    console.log("Generate data successfully.");
  });
})();
