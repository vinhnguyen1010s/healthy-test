const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("src/db.json");
const middlewares = jsonServer.defaults();
const queryString = require("query-string");
const PORT = process.env.PORT || 8081;

server.use(middlewares);

server.get("/echo", (req, res) => {
  res.jsonp(req.query);
});

server.use(jsonServer.bodyParser);

router.render = (req, res) => {
  const headers = res.getHeaders();

  const totalCountHeader = headers["x-total-count"];
  if (req.method === "GET" && totalCountHeader) {
    const queryParams = queryString.parse(req._parsedUrl.query);
    const totalPages = Math.ceil(
      Number.parseInt(totalCountHeader) / Number.parseInt(queryParams._limit)
    );

    const result = {
      data: res.locals.data,
      pagination: {
        _page: Number.parseInt(queryParams._page) || 1,
        _limit: Number.parseInt(queryParams._limit) || 8,
        _totalRows: Number.parseInt(totalCountHeader),
        // _totalPages: Number.parseInt(totalPages),
      },
    };

    return res.jsonp(result);
  }

  res.jsonp(res.locals.data);
};

// Use default router
server.use("/api", router);

server.listen(PORT, () => {
  console.log("JSON Server is running ...", PORT);
});
