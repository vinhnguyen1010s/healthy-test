# install project

### npm install

# If 'npm install' error can use 'npm install --force'

### npm install --force

# generator data json

### npm run generator

# start server

### npm start

# user client can test api by file test.http with postman
